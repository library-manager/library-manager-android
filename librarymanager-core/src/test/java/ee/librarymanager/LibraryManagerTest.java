package ee.librarymanager;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import ee.librarymanager.model.Book;
import ee.librarymanager.model.Borrowing;
import ee.librarymanager.model.Person;
import ee.librarymanager.repository.BookRepository;
import ee.librarymanager.repository.BorrowingRepository;
import ee.librarymanager.repository.PersonRepository;
import ee.librarymanager.service.BookLookupException;
import ee.librarymanager.service.BookLookupService;
import ee.librarymanager.service.IdCodeLookupException;
import ee.librarymanager.service.PersonLookupService;
import ee.librarymanager.util.Logger;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LibraryManagerTest {

    public static final String ISBN = "1234567891234";
    public static final String IDCODE = "36603150241";
    public static final String IDFORPERSONMOCK = "11111111111";
    public static final String PERSONFIRSTNAME = "Person";
    public static final String PERSONLASTNAME = "Name";

    @Mock
    private BookLookupService bookLookupService;

    @Mock
    private PersonLookupService personLookupService;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private PersonRepository personRepository;

    @Mock
    private BorrowingRepository borrowingRepository;

    @Mock
    private Logger logger;

    @InjectMocks
    LibraryManager manager;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void whenNoIDCodeThenShouldShowError() {
        // act
        manager.borrow();

        // assert
        assertThat(manager.getError(), is(equalTo("Please enter or scan ID-code")));
    }

    @Test
    public void whenISBNIsNullThenShouldShowError() throws IdCodeLookupException {
        // arrange
        createPersonMock(IDFORPERSONMOCK);

        // act
        manager.fetchPersonByIdCode(IDFORPERSONMOCK);
        manager.borrow();

        // assert
        assertThat(manager.getError(), is(equalTo("Please enter or scan ISBN")));
    }

    @Test
    public void whenISBNIsEmptyThenShouldShowError() {
        // act
        manager.fetchBookByISBN("");

        // assert
        assertThat(manager.getError(), is(equalTo("Please enter or scan ISBN")));
    }

    @Test
    public void whenISBNIsNotNumberThenShouldShowError() {
        // act
        manager.fetchBookByISBN("12345hello");

        // assert
        assertThat(manager.getError(), is(equalTo("ISBN must be a number with 10 or 13 digits")));
    }

    @Test
    public void whenISBNIsTooShortThenShouldShowError() {
        // act
        manager.fetchBookByISBN("12345");

        // assert
        assertThat(manager.getError(), is(equalTo("ISBN must be a number with 10 or 13 digits")));
    }

    @Test
    public void whenISBNIsTooLongThenShouldShowError() {
        // act
        manager.fetchBookByISBN("12345678912345");

        // assert
        assertThat(manager.getError(), is(equalTo("ISBN must be a number with 10 or 13 digits")));
    }

    @Test
    public void whenISBNIsInBetweenThenShouldShowError() {
        // act
        manager.fetchBookByISBN("123456789123");

        // assert
        assertThat(manager.getError(), is(equalTo("ISBN must be a number with 10 or 13 digits")));
    }

    @Test
    public void whenISBNContainsDashesThenShouldSucceed() throws BookLookupException {
        // act
        manager.fetchBookByISBN("12-3456-7891-234");

        // assert
        verify(bookLookupService).fetchBookByISBN("1234567891234");
    }

    @Test
    public void whenBookISBN10GivenThenFetchBookDetails() throws BookLookupException {
        // act
        manager.fetchBookByISBN("1234567891");

        // assert
        verify(bookLookupService).fetchBookByISBN("1234567891");
    }

    @Test
    public void whenBookISBN13GivenThenFetchBookDetails() throws BookLookupException {
        // act
        manager.fetchBookByISBN(ISBN);

        // assert
        verify(bookLookupService).fetchBookByISBN(ISBN);
    }

    @Test
    public void whenFetchBookSucceedsThenSaveBookToDatabase() throws BookLookupException {
        // arrange
        final Book book = createBookMock(ISBN);

        // act
        manager.fetchBookByISBN(ISBN);

        //assert
        verify(bookRepository).save(book);
    }

    @Test
    public void whenBorrowingSucceedsThenSavePersonToDatabase() throws BookLookupException, IdCodeLookupException {
        // arrange
        final Person person = createPersonMock(IDFORPERSONMOCK);
        createBookMock(ISBN);

        // act
        manager.fetchPersonByIdCode(IDFORPERSONMOCK);
        manager.fetchBookByISBN(ISBN);
        manager.borrow();

        //assert
        verify(personRepository).save(person);
    }

    @Test
    public void whenBorrowingSucceedsAndBookTitleIsNullThenShowsSuccessMessageWithEmptyTitle() throws BookLookupException, IdCodeLookupException {
        // arrange
        createBookMock(ISBN);
        createPersonMock(IDFORPERSONMOCK);


        // act
        manager.fetchPersonByIdCode(IDFORPERSONMOCK);
        manager.fetchBookByISBN(ISBN);
        manager.borrow();

        //assert
        assertThat(manager.getMessage(), is(equalTo("Hello Person Name! You successfully borrowed ''. Enjoy!")));
        assertThat(manager.borrowingWasSuccessful(), is(true));
    }

    @Test
    public void whenBorrowingSucceedsThenShowsSuccessMessageWithPersonNameAndBookTitle() throws BookLookupException, IdCodeLookupException {
        // arrange
        Book book = createBookMock(ISBN);
        book.setTitle("NameOfBook");
        createPersonMock(IDFORPERSONMOCK);

        // act
        manager.fetchBookByISBN(ISBN);
        manager.fetchPersonByIdCode(IDFORPERSONMOCK);
        manager.borrow();

        //assert
        assertThat(manager.getMessage(), is(equalTo("Hello Person Name! You successfully borrowed 'NameOfBook'. Enjoy!")));
        assertThat(manager.borrowingWasSuccessful(), is(true));
    }

    @Test
    public void afterErrorResetsTheErrorMessage() throws BookLookupException {
        // arrange
        final Book book = new Book();
        when(bookLookupService.fetchBookByISBN("9780834800793")).thenReturn(book);
        when(bookLookupService.fetchBookByISBN("9780834800794")).thenThrow(new BookLookupException("error"));


        // act
        manager.fetchBookByISBN("9780834800794");
        assertThat(manager.getError(), is(notNullValue()));
        manager.fetchBookByISBN("9780834800793");

        //assert
        assertThat(manager.getError(), is(nullValue()));
    }

    @Test
    public void whenFetchBookSucceedsThenShowFoundBookMessage() throws BookLookupException {
        // arrange
        final Book book = createBookMock(ISBN);
        book.setTitle("Whatever");

        // act
        manager.fetchBookByISBN(ISBN);

        //assert
        assertThat(manager.getMessage(), is(equalTo("Found book 'Whatever'")));
    }

    @Test
    public void whenIdCodeIsNotNumberThenShouldShowError() {
        // act
        manager.fetchPersonByIdCode("12345hello");

        // assert
        assertThat(manager.getError(), is(equalTo("ID-Code must be a number with 11 digits")));
    }

    @Test
    public void whenIdCodeIsTooShortThenShouldShowError() {
        // act
        manager.fetchPersonByIdCode("12345");

        // assert
        assertThat(manager.getError(), is(equalTo("ID-Code must be a number with 11 digits")));
    }

    @Test
    public void whenPersonIdCodeIsGivenThenFetchPerson() throws IdCodeLookupException {
        // act
        manager.fetchPersonByIdCode(IDCODE);

        // assert
        verify(personLookupService).fetchPersonByIdCode(IDCODE);
    }

    @Test
    public void whenPersonIdCodeIsGivenThenFetchPersonDetails() throws IdCodeLookupException {
        // act
        manager.fetchPersonByIdCode("36603150241");

        // assert
        verify(personLookupService).fetchPersonByIdCode("36603150241");
    }

    @Test
    public void whenFetchPersonSucceedsThenShowFoundPersonMessage() throws IdCodeLookupException {
        // arrange
        final Person person = new Person();
        person.setFirstName("This");
        person.setLastName("Whatever");
        when(personLookupService.fetchPersonByIdCode(IDCODE)).thenReturn(person);

        // act
        manager.fetchPersonByIdCode(IDCODE);

        //assert
        assertThat(manager.getMessage(), is(equalTo("Found person 'This Whatever'")));
    }

    @Test
    public void whenNamesWithCapitalsThenConvertToGoodFormat() throws IdCodeLookupException {
        // arrange
        final Person person = new Person();
        person.setFirstName("THIS IS SOMEONE");
        person.setLastName("WHATEVER");
        when(personLookupService.fetchPersonByIdCode(IDCODE)).thenReturn(person);

        // act
        manager.fetchPersonByIdCode(IDCODE);

        //assert
        assertThat(manager.getMessage(), is(equalTo("Found person 'This Is Someone Whatever'")));
    }

    @Test
    public void whenBorrowingSucceedsThenSaveBorrowing() throws IdCodeLookupException, BookLookupException {
        // arrange
        final Person person = createPersonMock(IDFORPERSONMOCK);
        final Book book = createBookMock(ISBN);

        // act
        manager.fetchPersonByIdCode(IDFORPERSONMOCK);
        manager.fetchBookByISBN(ISBN);
        manager.borrow();

        // assert
        assertThat(manager.borrowingWasSuccessful(), is(true));
        final ArgumentCaptor<Borrowing> borrowingCaptor = ArgumentCaptor.forClass(Borrowing.class);
        verify(borrowingRepository).save(borrowingCaptor.capture());
        final Borrowing borrowing = borrowingCaptor.getValue();
        assertThat(borrowing.getBook(), is(equalTo(book)));
        assertThat(borrowing.getPerson(), is(equalTo(person)));
    }

    private Person createPersonMock(String idforpersonmock) throws IdCodeLookupException {
        final Person person = new Person();
        person.setIdCode(idforpersonmock);
        person.setFirstName(PERSONFIRSTNAME);
        person.setLastName(PERSONLASTNAME);
        when(personLookupService.fetchPersonByIdCode(idforpersonmock)).thenReturn(person);
        return person;
    }

    private Book createBookMock(String isbn) throws BookLookupException {
        final Book book = new Book();
        book.setIsbn(isbn);
        when(bookLookupService.fetchBookByISBN(isbn)).thenReturn(book);
        return book;
    }
}
