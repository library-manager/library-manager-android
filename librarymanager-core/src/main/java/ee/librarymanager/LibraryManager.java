package ee.librarymanager;

import com.google.common.base.Joiner;

import java.util.Date;

import ee.librarymanager.model.Book;
import ee.librarymanager.model.Borrowing;
import ee.librarymanager.model.Person;
import ee.librarymanager.repository.BookRepository;
import ee.librarymanager.repository.BorrowingRepository;
import ee.librarymanager.repository.PersonRepository;
import ee.librarymanager.service.BookLookupException;
import ee.librarymanager.service.BookLookupService;
import ee.librarymanager.service.IdCodeLookupException;
import ee.librarymanager.service.PersonLookupService;
import ee.librarymanager.util.Logger;

import static com.google.common.base.Strings.isNullOrEmpty;

public class LibraryManager {

    private final BookLookupService bookLookupService;
    private final BookRepository bookRepository;
    private final PersonLookupService personLookupService;
    private final PersonRepository personRepository;
    private final BorrowingRepository borrowingRepository;
    private final Logger logger;
    private String error;
    private String message;
    private Book book;
    private Person person;

    public LibraryManager(BookLookupService bookLookupService, BookRepository bookRepository, PersonLookupService personLookupService, PersonRepository personRepository, BorrowingRepository borrowingRepository, Logger logger) {
        this.bookLookupService = bookLookupService;
        this.bookRepository = bookRepository;
        this.personLookupService = personLookupService;
        this.personRepository = personRepository;
        this.borrowingRepository = borrowingRepository;
        this.logger = logger;
    }

    public void borrow() {
        resetErrorAndMessage();
        try {
            if (person == null) {
                error = "Please enter or scan ID-code";
                return;
            }
            if (book == null) {
                error = "Please enter or scan ISBN";
                return;
            }

            savePerson();
            saveBorrowing();
            message = Joiner.on("").skipNulls().join("Hello ", person.getFullName(), "! You successfully borrowed '", book.getTitle(), "'. Enjoy!");
            resetBook();
            resetPerson();
        } catch (Exception e) {
            logger.error("LibraryManager.borrow", e.toString());
            error = "System error occurred";
        }
    }

    public void fetchBookByISBN(String isbn) {
        resetErrorAndMessage();
        resetBook();
        try {
            if (isNullOrEmpty(isbn)) {
                error = "Please enter or scan ISBN";
                return;
            }
            isbn = isbn.replace("-", "");
            if (!isbn.matches("^\\d{10}|\\d{13}$")) {
                error = "ISBN must be a number with 10 or 13 digits";
                return;
            }

            final Book book = bookLookupService.fetchBookByISBN(isbn);
            bookRepository.save(book);
            this.book = book;
            message = "Found book '" + book.getTitle() + "'";
        } catch (BookLookupException e) {
            error = e.getMessage();
        } catch (Exception e) {
            logger.error("LibraryManager.fetchBookByISBN", e.toString());
            error = "System error occurred";
        }
    }

    public void fetchPersonByIdCode(String idCode) {
        resetErrorAndMessage();
        resetPerson();
        try {
            if (isNullOrEmpty(idCode)) {
                error = "Please enter or scan ID-Code";
                return;
            }
            if (!idCode.matches("^\\d{11}$")) {
                error = "ID-Code must be a number with 11 digits";
                return;
            } else {
                final Person person = personLookupService.fetchPersonByIdCode(idCode);
                this.person = person;
                message = "Found person '" + person.getFullName() + "'";
            }
        } catch (IdCodeLookupException e) {
            error = e.getMessage();
        } catch (Exception e) {
            logger.error("LibraryManager.fetchPersonByIdCode", e.toString());
            error = "System error occurred";
        }
    }

    private void savePerson() {
        personRepository.save(person);
    }

    private void saveBorrowing() {
        final Borrowing borrowing = new Borrowing();
        borrowing.setBook(book);
        borrowing.setPerson(person);
        borrowing.setDate(new Date());
        borrowingRepository.save(borrowing);
    }

    private void resetPerson() {
        person = null;
    }

    private void resetBook() {
        book = null;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    private void resetErrorAndMessage() {
        error = null;
        message = null;
    }

    public Book getBook() {
        return book;
    }

    public boolean borrowingWasSuccessful() {
        return error == null && message != null;
    }

    public Person getPerson() {
        return person;
    }
}
