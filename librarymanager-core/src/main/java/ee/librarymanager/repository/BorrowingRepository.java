package ee.librarymanager.repository;

import ee.librarymanager.model.Borrowing;

public interface BorrowingRepository {
    void save(Borrowing borrowing);
}
