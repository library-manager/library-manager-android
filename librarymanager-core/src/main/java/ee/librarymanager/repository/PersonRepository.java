package ee.librarymanager.repository;

import ee.librarymanager.model.Person;

public interface PersonRepository {
    void save(Person person);
}
