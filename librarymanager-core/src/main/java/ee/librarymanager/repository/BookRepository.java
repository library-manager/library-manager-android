package ee.librarymanager.repository;

import ee.librarymanager.model.Book;

public interface BookRepository {
    void save(Book book);

    Book findByIsbn(String isbn);
}
