package ee.librarymanager.service;

public class BookLookupException extends Exception {
    public BookLookupException(String message) {
        super(message);
    }
}
