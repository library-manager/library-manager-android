package ee.librarymanager.service;

import ee.librarymanager.model.Person;

public interface PersonLookupService {
    Person fetchPersonByIdCode(String idCode) throws IdCodeLookupException;
}
