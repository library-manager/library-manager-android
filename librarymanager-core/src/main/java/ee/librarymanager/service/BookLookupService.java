package ee.librarymanager.service;

import ee.librarymanager.model.Book;

public interface BookLookupService {
    Book fetchBookByISBN(String isbn) throws BookLookupException;
}
