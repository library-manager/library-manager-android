package ee.librarymanager.service;


public class IdCodeLookupException extends Exception {
    public IdCodeLookupException(String message) {
        super(message);
    }
}
