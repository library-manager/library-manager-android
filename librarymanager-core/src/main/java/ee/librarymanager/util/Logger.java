package ee.librarymanager.util;


public interface Logger {
    void error(String tag, String message);
}
