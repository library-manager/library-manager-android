package ee.librarymanager.view;

public interface MainView {
    void showError(String message);

    void showMessage(String message);

    String getIDCode();

    String getISBN();

}
