package ee.librarymanager.model;

import com.google.common.base.Joiner;

public class Person {
    private String firstName;
    private String lastName;
    private String idCode;
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        lastName = decapitalizeName(lastName);
        firstName = decapitalizeName(firstName);
        return firstName + " " + lastName;
    }

    private String decapitalizeName(String name) {
        String subName;
        final String[] names = name.split(" ");

        for (int i = 0; i < names.length; i++) {
            subName = names[i];
            subName = subName.substring(0, 1).toUpperCase() + subName.substring(1).toLowerCase();
            names[i] = subName;
        }

        name = Joiner.on(" ").join(names);
        return name;
    }

}
