package ee.librarymanager;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Joiner;

import ee.librarymanager.model.Book;
import ee.librarymanager.model.Person;
import ee.librarymanager.repository.BookRepository;
import ee.librarymanager.repository.BorrowingRepository;
import ee.librarymanager.repository.PersonRepository;
import ee.librarymanager.repository.firestore.FirestoreBookRepositoryImpl;
import ee.librarymanager.repository.firestore.FirestoreBorrowingRepositoryImpl;
import ee.librarymanager.repository.firestore.FirestorePersonRepositoryImpl;
import ee.librarymanager.service.BookLookupService;
import ee.librarymanager.service.BookLookupServiceImpl;
import ee.librarymanager.service.PersonLookupService;
import ee.librarymanager.service.PersonLookupServiceImpl;
import ee.librarymanager.util.LoggerImpl;

public class MainActivity extends AppCompatActivity {

    private LibraryManager libraryManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(ee.librarymanager.R.layout.activity_main);

        final BookRepository bookRepository = new FirestoreBookRepositoryImpl();
        final PersonRepository personRepository = new FirestorePersonRepositoryImpl();
        final BorrowingRepository borrowingRepository = new FirestoreBorrowingRepositoryImpl();
        final BookLookupService bookLookupService = new BookLookupServiceImpl();
        final PersonLookupService personLookupService = new PersonLookupServiceImpl();
        final LoggerImpl logger = new LoggerImpl();
        libraryManager = new LibraryManager(bookLookupService, bookRepository, personLookupService, personRepository, borrowingRepository, logger);

        final EditText enterBookIsbnField = (EditText) findViewById(R.id.EnterBookIsbnField);
        final EditText enterIdCodeField = (EditText) findViewById(R.id.EnterIdCodeField);

        enterIdCodeField.setOnEditorActionListener(new TaskExecutorOnEditorActionListener(enterIdCodeField, FetchTaskType.PERSON));

        enterBookIsbnField.setOnEditorActionListener(new TaskExecutorOnEditorActionListener(enterBookIsbnField, FetchTaskType.BOOK));

        final Button borrowTheBookButton = (Button) findViewById(R.id.BorrowTheBookButton);
        borrowTheBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BorrowBookTask().execute(libraryManager);
            }
        });
    }

    private void showError(String message) {
        Toast myToast = Toast.makeText(getApplicationContext(),
            message,
            Toast.LENGTH_LONG);
        myToast.show();
    }

    private void showMessage(String message) {
        showError(message);
    }

    private String getIDCode() {
        final EditText idCodeField = (EditText) findViewById(R.id.EnterIdCodeField);
        return idCodeField.getText().toString();
    }

    private String getISBN() {
        final EditText isbnField = (EditText) findViewById(R.id.EnterBookIsbnField);
        return isbnField.getText().toString();
    }

    private void showErrorOrMessage(LibraryManager libraryManager) {
        if (libraryManager.getError() != null) {
            showError(libraryManager.getError());
        } else {
            showMessage(libraryManager.getMessage());
        }
    }

    class BorrowBookTask extends AsyncTask<LibraryManager, Void, LibraryManager> {

        @Override
        protected LibraryManager doInBackground(LibraryManager... params) {
            final LibraryManager libraryManager = params[0];
            libraryManager.borrow();
            return libraryManager;
        }

        @Override
        protected void onPostExecute(LibraryManager libraryManager) {
            showErrorOrMessage(libraryManager);
            if (libraryManager.borrowingWasSuccessful()) {
                resetPersonNameField();
                resetBookTitleAndAuthorFields();
                resetEntryFields();
            }
        }
    }

    class FetchBookTask extends AsyncTask<LibraryManager, Void, LibraryManager> {

        @Override
        protected LibraryManager doInBackground(LibraryManager... params) {
            final LibraryManager libraryManager = params[0];
            libraryManager.fetchBookByISBN(getISBN());
            return libraryManager;
        }

        @Override
        protected void onPostExecute(LibraryManager libraryManager) {
            showErrorOrMessage(libraryManager);
            final Book book = libraryManager.getBook();
            if (book != null) {
                fillBookTitleAndAuthorFields(book);
            } else {
                resetBookTitleAndAuthorFields();
            }
        }
    }

    class FetchPersonTask extends AsyncTask<LibraryManager, Void, LibraryManager> {

        @Override
        protected LibraryManager doInBackground(LibraryManager... params) {
            final LibraryManager libraryManager = params[0];
            libraryManager.fetchPersonByIdCode(getIDCode());
            return libraryManager;
        }

        @Override
        protected void onPostExecute(LibraryManager libraryManager) {
            showErrorOrMessage(libraryManager);
            final Person person = libraryManager.getPerson();
            if (person != null) {
                fillPersonNameField(person);
            } else {
                resetPersonNameField();
            }
        }
    }

    private void resetEntryFields() {
        final EditText enterIdField = (EditText) findViewById(R.id.EnterIdCodeField);
        final EditText enterIsbnField = (EditText) findViewById(R.id.EnterBookIsbnField);
        enterIdField.setText("");
        enterIsbnField.setText("");
    }

    private void resetPersonNameField() {
        final TextView nameTextField = (TextView) findViewById(R.id.NameTextField);
        nameTextField.setText("");
    }

    private void fillPersonNameField(Person person) {
        final TextView nameTextField = (TextView) findViewById(R.id.NameTextField);
        nameTextField.setText(getString(R.string.name) + person.getFullName());
    }

    private void fillBookTitleAndAuthorFields(Book book) {
        final TextView bookNameTextField = (TextView) findViewById(R.id.bookNameTextField);
        final TextView bookAuthorTextField = (TextView) findViewById(R.id.bookAuthorTextField);
        bookNameTextField.setText(getString(R.string.bookTitle) + book.getTitle());
        bookAuthorTextField.setText(getString(R.string.authors) + Joiner.on(", ").join(book.getAuthors()));
    }

    private void resetBookTitleAndAuthorFields() {
        final TextView bookNameTextField = (TextView) findViewById(R.id.bookNameTextField);
        final TextView bookAuthorTextField = (TextView) findViewById(R.id.bookAuthorTextField);
        bookNameTextField.setText("");
        bookAuthorTextField.setText("");
    }

    enum FetchTaskType {BOOK, PERSON}

    private AsyncTask<LibraryManager, Void, LibraryManager> createFetchTask(FetchTaskType type) {
        switch (type) {
            case BOOK:
                return new FetchBookTask();
            case PERSON:
                return new FetchPersonTask();
            default:
                throw new RuntimeException("Unknown FetchTaskType");
        }
    }

    private class TaskExecutorOnEditorActionListener implements EditText.OnEditorActionListener {
        private final EditText editText;
        private final FetchTaskType taskType;

        public TaskExecutorOnEditorActionListener(EditText editText, FetchTaskType taskType) {
            this.editText = editText;
            this.taskType = taskType;
        }

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                createFetchTask(taskType).execute(libraryManager);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                editText.clearFocus();
                imm.hideSoftInputFromWindow(editText.getApplicationWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
                return true;
            }
            return false;
        }
    }
}
