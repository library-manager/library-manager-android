package ee.librarymanager.util;


import android.util.Log;

public class LoggerImpl implements Logger {
    @Override
    public void error(String tag, String message) {
        Log.e(tag, message);
    }
}
