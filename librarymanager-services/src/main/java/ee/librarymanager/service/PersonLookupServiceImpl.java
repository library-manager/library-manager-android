package ee.librarymanager.service;


import com.google.common.base.CharMatcher;
import com.google.common.io.BaseEncoding;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import ee.librarymanager.model.Person;
import ee.librarymanager.service.idcode.IdCodeApi;
import ee.librarymanager.service.idcode.Results;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PersonLookupServiceImpl implements PersonLookupService {
    @Override
    public Person fetchPersonByIdCode(String idCode) throws IdCodeLookupException {
        final IdCodeApi idCodeApi = connect();
        try {
            final Results apiResponse = idCodeApi.findPersonByIdCode(idCode).execute().body();
            if (apiResponse == null || apiResponse.getKeys().size() < 1) {
                throw new IdCodeLookupException("No person found for ID-Code " + idCode);
            }
            return extractPersonFromCertificate(apiResponse.getKeys().get(0));
        } catch (IOException e) {
            if (e.getMessage() == "status 400") {
                throw new IdCodeLookupException("No person found for ID-Code " + idCode);
            } else {
                throw new IdCodeLookupException("API response error: " + e.getMessage());
            }
        } catch (CertificateException e) {
            throw new IdCodeLookupException("Invalid certificate : " + e.getMessage());
        }
    }

    private Person extractPersonFromCertificate(String pem) throws CertificateException, IdCodeLookupException {
        final X509Certificate certificate = convertToX509Cert(pem);
        final Person person = new Person();
        final String name = certificate.getSubjectX500Principal().getName();
        final Pattern pattern = Pattern.compile(".+CN=(.+)\\\\,(.+)\\\\,([^,]+),.+");
        final Matcher matcher = pattern.matcher(name);
        if (!matcher.matches()) {
            throw new IdCodeLookupException("Invalid certificate subject string");
        }
        person.setFirstName(matcher.group(2));
        person.setLastName(matcher.group(1));
        person.setIdCode(matcher.group(3));
        return person;
    }

    static X509Certificate convertToX509Cert(String certificateString) throws CertificateException {
        if (certificateString == null || certificateString.trim().isEmpty()) {
            throw new CertificateException("Empty certificate string");
        }
        certificateString = certificateString.replace("-----BEGIN CERTIFICATE-----", "")
            .replace("-----END CERTIFICATE-----", "");
        certificateString = CharMatcher.whitespace().removeFrom(certificateString);
        byte[] certificateData = BaseEncoding.base64().decode(certificateString);
        final CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");
        return (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(certificateData));
    }

    private static IdCodeApi connect() {
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://isikukood.ee")
            .addConverterFactory(GsonConverterFactory.create())
            .client(getUnsafeOkHttpClient())
            .build();

        return retrofit.create(IdCodeApi.class);
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[]{};
                    }
                }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
