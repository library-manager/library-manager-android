package ee.librarymanager.service.googlebooks;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleBooksApi {
    @GET ("/books/v1/volumes")
    Call<Results> findBookByISBN(@Query("q") String isbn);
}
