package ee.librarymanager.service.googlebooks;


import java.util.List;

class VolumeInfo {
    private String title;

    private List<String> authors;

    private List<IndustryIdentifier> industryIdentifiers;

    String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    List<String> getAuthors() {
        return authors;
    }

    void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    List<IndustryIdentifier> getIndustryIdentifiers() {
        return industryIdentifiers;
    }

    void setIndustryIdentifiers(List<IndustryIdentifier> industryIdentifiers) {
        this.industryIdentifiers = industryIdentifiers;
    }

    String getIsbn13() {
        for (IndustryIdentifier identifier : industryIdentifiers) {
            if (identifier.getType().equals("ISBN_13")) {
                return identifier.getIdentifier();
            }
        }
        throw new RuntimeException("Did not find ISBN_13");
    }
}
