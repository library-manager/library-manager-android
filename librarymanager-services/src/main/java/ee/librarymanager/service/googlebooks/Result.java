package ee.librarymanager.service.googlebooks;

import ee.librarymanager.model.Book;

public class Result {

    private VolumeInfo volumeInfo;

    public Book getBook() {
        final Book book = new Book();
        book.setTitle(volumeInfo.getTitle());
        book.setAuthors(volumeInfo.getAuthors());
        book.setIsbn(volumeInfo.getIsbn13());
        return book;
    }

}
