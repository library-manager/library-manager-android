package ee.librarymanager.service.googlebooks;


class IndustryIdentifier {
    private String type;
    private String identifier;

    String getType() {
        return type;
    }

    void setType(String type) {
        this.type = type;
    }

    String getIdentifier() {
        return identifier;
    }

    void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
