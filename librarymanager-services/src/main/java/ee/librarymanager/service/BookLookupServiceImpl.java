package ee.librarymanager.service;

import java.io.IOException;
import java.util.List;

import ee.librarymanager.model.Book;
import ee.librarymanager.service.googlebooks.GoogleBooksApi;
import ee.librarymanager.service.googlebooks.Result;
import ee.librarymanager.service.googlebooks.Results;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BookLookupServiceImpl implements BookLookupService {

    @Override
    public Book fetchBookByISBN(String isbn) throws BookLookupException {
        final GoogleBooksApi googleBooksApi = connect();
        try {
            final Results apiResponse = googleBooksApi.findBookByISBN("isbn:" + isbn).execute().body();
            if (apiResponse == null || apiResponse.getTotalItems() < 1) {
                throw new BookLookupException("No books found for ISBN " + isbn);
            }
            final List<Result> results = apiResponse.getItems();
            if (results == null || results.size() < 1) {
                throw new BookLookupException("Invalid items list for ISBN " + isbn);
            }
            final Book book = results.get(0).getBook();
            return book;
        } catch (IOException e) {
            if (e.getMessage() == "status 400") {
                throw new BookLookupException("No books found for ISBN " + isbn);
            } else {
                throw new BookLookupException("API response error: " + e.getMessage());
            }
        }
    }

    private static GoogleBooksApi connect() {
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.googleapis.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

        return retrofit.create(GoogleBooksApi.class);
    }
}
