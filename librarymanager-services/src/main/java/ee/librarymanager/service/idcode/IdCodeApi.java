package ee.librarymanager.service.idcode;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IdCodeApi {
    @GET("/{idCode}/all/pem/json")
    Call<Results> findPersonByIdCode(@Path("idCode") String idCode);

}
