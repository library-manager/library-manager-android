package ee.librarymanager.service;

import org.junit.Test;

import java.security.cert.CertificateException;

import ee.librarymanager.model.Person;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PersonLookupServiceImplTest {
    private static final String TEST_CERTIFICATE = "-----BEGIN CERTIFICATE-----" +
        "MIIEBTCCAu2gAwIBAgIQIAemLuh2ljFWywrSCv+tyjANBgkqhkiG9w0BAQsFADBkMQswCQYDVQQG" +
        "EwJFRTEiMCAGA1UECgwZQVMgU2VydGlmaXRzZWVyaW1pc2tlc2t1czEXMBUGA1UEAwwORVNURUlE" +
        "LVNLIDIwMTExGDAWBgkqhkiG9w0BCQEWCXBraUBzay5lZTAeFw0xNjAyMjIxMzE5MTRaFw0yMTAy" +
        "MjIyMTU5NTlaMIGdMQswCQYDVQQGEwJFRTEbMBkGA1UECgwSRVNURUlEIChNT0JJSUwtSUQpMRcw" +
        "FQYDVQQLDA5hdXRoZW50aWNhdGlvbjEhMB8GA1UEAwwYVklJTFVQLEhBUkRZLDM2NjAzMTUwMjQx" +
        "MQ8wDQYDVQQEDAZWSUlMVVAxDjAMBgNVBCoMBUhBUkRZMRQwEgYDVQQFEwszNjYwMzE1MDI0MTBZ" +
        "MBMGByqGSM49AgEGCCqGSM49AwEHA0IABOhwduzonFGRBhAV8LbeQNkFX0roH+0YT7C6lBFvrXY0" +
        "xHI90RreV8lzE9z68tq50oxgq3AUmvQBn8fA8IgKV46jggFCMIIBPjAJBgNVHRMEAjAAMA4GA1Ud" +
        "DwEB/wQEAwIEsDBbBgNVHSAEVDBSMFAGCisGAQQBzh8BAwMwQjAdBggrBgEFBQcCAjARDA9Db250" +
        "cmFjdCAxLjExLTkwIQYIKwYBBQUHAgEWFWh0dHBzOi8vd3d3LnNrL\nmVlL2NwczAgBgNVHREEGTAX" +
        "gRVoYXJkeS52aWlsdXBAZWVzdGkuZWUwHQYDVR0OBBYEFMNSalLCoro9zDjmUSGWDvXhGx0wMCAG\n" +
        "A1UdJQEB/wQWMBQGCCsGAQUFBwMCBggrBgEFBQcDBDAfBgNVHSMEGDAWgBR7avJVUFy42XoIh0Gu" +
        "+qIrPVtXdjBABgNVHR8EOTA3MDWgM6Axhi9odHRwOi8vd3d3LnNrLmVlL3JlcG9zaXRvcnkvY3Js" +
        "cy9lc3RlaWQyMDExLmNybDANBgkqhkiG9w0BAQsFAAOCAQEAoeYtnxsDzisKKw61z03rrsXQXQPO" +
        "P3fuAQQCCf+gLyN/VROBBpiZf7i4GYpCey+n+/OX/hwkpOwsG5fDpsv6ocKcD4DOf/zObHUxpC7z" +
        "pSz7jnigABZUCr4yJJbYLN1G8miOVGAtUhZ0Tnd3BDCzEe5R5e1MgvT9LNORyHxR5UJwJROx1roC" +
        "YkIrjANDQC1hX+VYcOF22C4yeBxR4vx/UIscRErPEib9roU9IYJHJw/WcYEQ/02aCvPPCKXudcMZ" +
        "O0H+XveVE3h4l/CtJKYjfNP6AoAvt2u0wwwftqXUjDRrtvwGEx1TQC0Y/3q0MB2yGNABXptP/Wnz" +
        "+iFBmWzHAQ==" +
        "-----END CERTIFICATE-----";
    private PersonLookupServiceImpl personLookupService = new PersonLookupServiceImpl();

    @Test
    public void whenFetchValidIdThenReturnsPerson() throws Exception {
        assertThat(personLookupService.fetchPersonByIdCode("36603150241").getFirstName(), is(equalTo("HARDY")));
    }

    @Test
    public void certificateIsDecodedCorrectly() throws CertificateException {
        PersonLookupServiceImpl.convertToX509Cert(TEST_CERTIFICATE);
    }

    @Test
    public void whenFetchPersonByIdCodeThenDetailsAreCorrect() throws Exception {
        final Person person = personLookupService.fetchPersonByIdCode("36603150241");
        assertThat(person.getFullName(), is(equalTo("Hardy Viilup")));
        assertThat(person.getIdCode(), is(equalTo("36603150241")));
    }
}
