package ee.librarymanager.service;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class BookLookupServiceImplTest {
    private BookLookupServiceImpl bookLookupService = new BookLookupServiceImpl();

    @Test
    public void whenSingleLookupResultThenReturnsIt() throws Exception {
        assertThat(bookLookupService.fetchBookByISBN("9781590308905").getTitle(), is(equalTo("Zen Mind, Beginner's Mind")));
    }

    @Test
    public void whenMultipleLookupResultsThenReturnsFirst() throws Exception {
        assertThat(bookLookupService.fetchBookByISBN("9780132778046").getTitle(), is(equalTo("Effective Java")));
    }

    @Test
    public void whenIsbnIsNullThenThrowsException() throws BookLookupException {
        assertThatThrownBy(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                bookLookupService.fetchBookByISBN(null);
            }
        }).isInstanceOf(BookLookupException.class)
            .hasMessageContaining("No books found for ISBN null");
    }

    @Test
    public void whenIsbnIsTooLongThenThrowsException() throws BookLookupException {
        assertThatThrownBy(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                bookLookupService.fetchBookByISBN("9781944345112978194434511297819443451129781944345112");
            }
        }).isInstanceOf(BookLookupException.class)
            .hasMessageContaining("No books found for ISBN");
    }

    @Test
    public void whenIsbnIsNotFoundThenThrowsException() throws BookLookupException {
        assertThatThrownBy(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                bookLookupService.fetchBookByISBN("9780834800794");
            }
        }).isInstanceOf(BookLookupException.class)
            .hasMessageContaining("No books found for ISBN");
    }

}
