package ee.librarymanager.repository.roomdatabase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;

import org.junit.Test;

import ee.librarymanager.model.Person;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RoomPersonRepositoryImplTest {
    @Test
    public void save() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        final RoomPersonRepositoryImpl personRepository = new RoomPersonRepositoryImpl(appContext);
        final Person person = createSaveAndGetPerson(personRepository);
        final Person personFromDB = personRepository.findByIdCode(person.getIdCode());
        assertThat(personFromDB.getIdCode(), is(equalTo(person.getIdCode())));
        assertThat(personFromDB.getLastName(), is(equalTo(person.getLastName())));
        assertThat(personFromDB.getFirstName(), is(equalTo(person.getFirstName())));
    }

    @NonNull
    static Person createSaveAndGetPerson(RoomPersonRepositoryImpl personRepository) {
        final Person person = new Person();
        person.setIdCode("1234");
        person.setFirstName("Peter");
        person.setLastName("Peet");
        personRepository.save(person);
        return person;
    }
}
