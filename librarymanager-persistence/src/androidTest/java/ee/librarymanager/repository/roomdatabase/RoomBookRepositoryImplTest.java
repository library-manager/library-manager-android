package ee.librarymanager.repository.roomdatabase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import ee.librarymanager.model.Book;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class RoomBookRepositoryImplTest {
    @Test
    public void testSaveBook() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        final RoomBookRepositoryImpl bookRepository = new RoomBookRepositoryImpl(appContext);
        final Book book = createSaveAndGetBook(bookRepository);
        final Book bookFromDB = bookRepository.findByIsbn(book.getIsbn());
        assertThat(bookFromDB.getTitle(), is(equalTo(book.getTitle())));
        assertThat(bookFromDB.getIsbn(), is(equalTo(book.getIsbn())));
        assertThat(bookFromDB.getAuthors(), is(equalTo(book.getAuthors())));
    }

    @NonNull
    static Book createSaveAndGetBook(RoomBookRepositoryImpl bookRepository) {
        final Book book = new Book();
        book.setIsbn("123");
        book.setTitle("Testbook");
        book.setAuthors(Arrays.asList("James Freak", "Paul , Test"));
        bookRepository.save(book);
        return book;
    }
}
