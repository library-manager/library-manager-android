package ee.librarymanager.repository.roomdatabase;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.junit.Test;

import java.util.Date;

import ee.librarymanager.model.Book;
import ee.librarymanager.model.Borrowing;
import ee.librarymanager.model.Person;

public class RoomBorrowingRepositoryImplTest {
    @Test
    public void save() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        final RoomBookRepositoryImpl bookRepository = new RoomBookRepositoryImpl(appContext);
        final RoomPersonRepositoryImpl personRepository = new RoomPersonRepositoryImpl(appContext);
        final Book book = RoomBookRepositoryImplTest.createSaveAndGetBook(bookRepository);
        final Person person = RoomPersonRepositoryImplTest.createSaveAndGetPerson(personRepository);
        final RoomBorrowingRepositoryImpl borrowingRepository = new RoomBorrowingRepositoryImpl(appContext);
        final Borrowing borrowing = new Borrowing();
        borrowing.setBook(book);
        borrowing.setPerson(person);
        borrowing.setDate(new Date());
        borrowingRepository.save(borrowing);
    }

}
