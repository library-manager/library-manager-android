package ee.librarymanager.repository.firestore;

import android.support.annotation.NonNull;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import ee.librarymanager.model.Book;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class FirestoreBookRepositoryImplTest {

    final FirestoreBookRepositoryImpl bookRepository = new FirestoreBookRepositoryImpl();

    @Test
    public void testSaveBook() throws Exception {
        final Book book = createSaveAndGetBook(bookRepository);
        final Book bookFromDB = bookRepository.findByIsbn(book.getIsbn());
        assertThat(bookFromDB.getTitle(), is(equalTo(book.getTitle())));
        assertThat(bookFromDB.getIsbn(), is(equalTo(book.getIsbn())));
        assertThat(bookFromDB.getAuthors(), is(equalTo(book.getAuthors())));
    }

    @Test
    public void testFindingNonexistingBook() {
        assertThat(bookRepository.findByIsbn("nonexistingisbn"), is(nullValue()));
    }

    @Test
    public void testFindingExistingBook() {
        assertThat(bookRepository.findByIsbn("123"), is(equalTo(getBook())));
    }

    @NonNull
    static Book createSaveAndGetBook(FirestoreBookRepositoryImpl bookRepository) {
        final Book book = getBook();
        bookRepository.save(book);
        return book;
    }

    @NonNull
    private static Book getBook() {
        final Book book = new Book();
        book.setIsbn("123");
        book.setTitle("Testbook");
        book.setAuthors(Arrays.asList("James Freak", "Paul , Test"));
        return book;
    }
}
