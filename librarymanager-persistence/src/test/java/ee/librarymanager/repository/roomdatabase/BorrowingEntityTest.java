package ee.librarymanager.repository.roomdatabase;


import org.junit.Test;

import java.util.Date;

import ee.librarymanager.model.Book;
import ee.librarymanager.model.Borrowing;
import ee.librarymanager.model.Person;
import ee.librarymanager.repository.roomdatabase.borrowing.BorrowingEntity;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class BorrowingEntityTest {
    @Test
    public void fromBorrowingEntityTest() {
        final Borrowing borrowing = new Borrowing();
        borrowing.setDate(new Date());
        final Person person = new Person();
        person.setIdCode("36603150241");
        borrowing.setPerson(person);
        final Book book = new Book();
        book.setIsbn("123");
        borrowing.setBook(book);
        final BorrowingEntity borrowingEntity = BorrowingEntity.fromBorrowing(borrowing);
        assertThat(borrowingEntity.getBookId(), is(equalTo("123")));
        assertThat(borrowingEntity.getPersonId(), is(equalTo("36603150241")));
    }
}
