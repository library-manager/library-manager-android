package ee.librarymanager.repository.roomdatabase;

import org.junit.Test;

import java.util.Arrays;

import ee.librarymanager.model.Book;
import ee.librarymanager.repository.roomdatabase.book.BookEntity;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class BookEntityTest {
    @Test
    public void fromBookTest() {
        final Book book = new Book();
        book.setIsbn("123");
        book.setTitle("Testbook");
        book.setAuthors(Arrays.asList("James Freak", "Paul , Test"));
        final BookEntity bookEntity = BookEntity.fromBook(book);
        assertThat(bookEntity.getTitle(), is(equalTo("Testbook")));
        assertThat(bookEntity.getIsbn(), is(equalTo("123")));
        assertThat(bookEntity.getAuthors(), is(equalTo("James Freak|Paul , Test")));
    }

    @Test
    public void toBookTest() {
        final BookEntity bookEntity = new BookEntity();
        bookEntity.setIsbn("123");
        bookEntity.setTitle("Testbook");
        bookEntity.setAuthors("Carl Freaky|Paul , goodTest");
        final Book book1 = bookEntity.toBook();
        assertThat(book1.getTitle(), is(equalTo("Testbook")));
        assertThat(book1.getIsbn(), is(equalTo("123")));
        assertThat(book1.getAuthors(), is(equalTo(Arrays.asList("Carl Freaky", "Paul , goodTest"))));
    }

}
