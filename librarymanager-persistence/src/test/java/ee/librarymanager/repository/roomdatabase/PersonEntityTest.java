package ee.librarymanager.repository.roomdatabase;


import org.junit.Test;

import ee.librarymanager.model.Person;
import ee.librarymanager.repository.roomdatabase.person.PersonEntity;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PersonEntityTest {

    @Test
    public void fromPersonTest() {
        final Person person = new Person();
        person.setIdCode("1234");
        person.setFirstName("Firstname");
        person.setLastName("Lastname");
        final PersonEntity personEntity = PersonEntity.fromPerson(person);
        assertThat(personEntity.getIdCode(), is(equalTo("1234")));
        assertThat(personEntity.getFirstName(), is(equalTo("Firstname")));
        assertThat(personEntity.getLastName(), is(equalTo("Lastname")));
    }

    @Test
    public void toPersonTest() {
        final PersonEntity personEntity = new PersonEntity();
        personEntity.setIdCode("5234");
        personEntity.setFirstName("FirstName");
        personEntity.setLastName("LastName");
        final Person person1 = personEntity.toPerson();
        assertThat(person1.getIdCode(), is(equalTo("5234")));
        assertThat(person1.getFirstName(), is(equalTo("FirstName")));
        assertThat(person1.getLastName(), is(equalTo("LastName")));
    }
}
