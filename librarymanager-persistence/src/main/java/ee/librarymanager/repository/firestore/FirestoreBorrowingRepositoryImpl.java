package ee.librarymanager.repository.firestore;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Transaction;

import java.util.concurrent.ExecutionException;

import javax.annotation.Nullable;

import ee.librarymanager.model.Borrowing;
import ee.librarymanager.repository.BorrowingRepository;

public class FirestoreBorrowingRepositoryImpl implements BorrowingRepository {

    @Override
    public void save(final Borrowing borrowing) {
        Log.e("firestore-br-save", "Starting to write borrowing book with ISBN " + borrowing.getBook().getIsbn()
            + " to person with first name " + borrowing.getPerson().getFirstName() + " to Firestore.");
        final BorrowingEntity borrowingEntity = BorrowingEntity.fromBorrowing(borrowing);
        final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        final DocumentReference borrowingWriteReference = firestore.collection("borrowing").document();
        final Task<Void> transactionTask = firestore.runTransaction(new Transaction.Function<Void>() {
            @Nullable
            @Override
            public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                transaction.set(borrowingWriteReference, borrowingEntity);
                Log.e("firestore-br-save", "Borrowing of book with ISBN " + borrowingEntity.getBookIsbn() + " : Firestore transaction successful");
                return null;
            }
        });
        try {
            Tasks.await(transactionTask);
        } catch (ExecutionException e) {
            Log.e("firestore-br-save", "Exception", e);
        } catch (InterruptedException e) {
            Log.e("firestore-br-save", "Exception", e);
        }
    }
}
