package ee.librarymanager.repository.firestore;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.annotation.Nullable;

import ee.librarymanager.model.Book;
import ee.librarymanager.repository.BookRepository;

public class FirestoreBookRepositoryImpl implements BookRepository {

    @Override
    public void save(final Book book) {
        Log.e("firestore-bk-save", "Starting to write book with ISBN " + book.getIsbn() + " to Firestore");
        final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        final DocumentReference bookWriteReference = firestore.collection("book").document();
        if (findByIsbn(book.getIsbn()) == null) {
            final Task<Void> transactionTask = firestore.runTransaction(new Transaction.Function<Void>() {
                @Nullable
                @Override
                public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                    transaction.set(bookWriteReference, book);
                    Log.e("firestore-bk-save", "book with ISBN " + book.getIsbn() + " : Firestore transaction successful");
                    return null;
                }
            });
            try {
                Tasks.await(transactionTask);
            } catch (ExecutionException e) {
                Log.e("firestore-bk-save", "Exception", e);
            } catch (InterruptedException e) {
                Log.e("firestore-bk-save", "Exception", e);
            }
        }
    }

    @Override
    public Book findByIsbn(String isbn) {
        final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        final CollectionReference bookReadReference = firestore.collection("book");
        Query query = bookReadReference.whereEqualTo("isbn", isbn);
        try {
            final QuerySnapshot querySnapshot = Tasks.await(query.get());
            final List<DocumentSnapshot> documents = querySnapshot.getDocuments();
            if (documents.isEmpty()) {
                return null;
            }
            return documents.get(0).toObject(Book.class);
        } catch (ExecutionException e) {
            Log.e("firestore-bk-findByIsbn", "Exception", e);
        } catch (InterruptedException e) {
            Log.e("firestore-bk-findByIsbn", "Exception", e);
        }
        return null;
    }
}
