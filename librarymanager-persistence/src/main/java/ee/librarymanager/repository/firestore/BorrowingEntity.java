package ee.librarymanager.repository.firestore;

import java.util.Date;

import ee.librarymanager.model.Borrowing;

class BorrowingEntity {
    private String bookIsbn;
    private String personIdCode;
    private Date date;

    public String getBookIsbn() {
        return bookIsbn;
    }

    public void setBookIsbn(String bookIsbn) {
        this.bookIsbn = bookIsbn;
    }

    public String getPersonIdCode() {
        return personIdCode;
    }

    public void setPersonIdCode(String personIdCode) {
        this.personIdCode = personIdCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    static BorrowingEntity fromBorrowing(Borrowing borrowing) {
        final BorrowingEntity borrowingEntity = new BorrowingEntity();
        borrowingEntity.setBookIsbn(borrowing.getBook().getIsbn());
        borrowingEntity.setPersonIdCode(borrowing.getPerson().getIdCode());
        borrowingEntity.setDate(borrowing.getDate());
        return borrowingEntity;
    }
}
