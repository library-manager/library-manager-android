package ee.librarymanager.repository.firestore;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.annotation.Nullable;

import ee.librarymanager.model.Person;
import ee.librarymanager.repository.PersonRepository;

public class FirestorePersonRepositoryImpl implements PersonRepository {

    @Override
    public void save(final Person person) {
        Log.e("firestore-ps-save", "Starting to write person with first name " + person.getFirstName() + " to Firestore");
        final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        final DocumentReference personWriteReference = firestore.collection("person").document();
        if (findByIdCode(person.getIdCode()) == null) {
            final Task<Void> transactionTask = firestore.runTransaction(new Transaction.Function<Void>() {
                @Nullable
                @Override
                public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                    transaction.set(personWriteReference, person);
                    Log.e("firestore-ps-save", "person with first name " + person.getFirstName() + " : Firestore transaction successful");
                    return null;
                }
            });
            try {
                Tasks.await(transactionTask);
            } catch (ExecutionException e) {
                Log.e("firestore-ps-save", "Exception", e);
            } catch (InterruptedException e) {
                Log.e("firestore-ps-save", "Exception", e);
            }
        }
    }

    public Person findByIdCode(String idCode) {
        final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        final CollectionReference personReadReference = firestore.collection("person");
        Query query = personReadReference.whereEqualTo("idCode", idCode);
        try {
            final QuerySnapshot querySnapshot = Tasks.await(query.get());
            final List<DocumentSnapshot> documents = querySnapshot.getDocuments();
            if (documents.isEmpty()) {
                return null;
            }
            return documents.get(0).toObject(Person.class);
        } catch (ExecutionException e) {
            Log.e("firestore-ps-findByIdC", "Exception", e);
        } catch (InterruptedException e) {
            Log.e("firestore-ps-findByIdC", "Exception", e);
        }
        return null;
    }
}
