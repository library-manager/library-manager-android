package ee.librarymanager.repository.roomdatabase;


import android.content.Context;

import ee.librarymanager.model.Book;
import ee.librarymanager.repository.BookRepository;
import ee.librarymanager.repository.roomdatabase.book.BookEntity;

public class RoomBookRepositoryImpl extends RepositoryImplBase implements BookRepository {

    public RoomBookRepositoryImpl(Context context) {
        super(context);
    }

    @Override
    public void save(Book book) {
        final BookEntity bookEntity = database.bookDao().findByIsbn(book.getIsbn());
        if (bookEntity == null) { // if it was not found
            database.bookDao().save(BookEntity.fromBook(book));
        }
    }

    @Override
    public Book findByIsbn(String isbn) {
        final BookEntity bookEntity = database.bookDao().findByIsbn(isbn);
        return bookEntity.toBook();
    }
}
