package ee.librarymanager.repository.roomdatabase;


import android.content.Context;

abstract class RepositoryImplBase {
    static LibraryManagerDatabase database;

    RepositoryImplBase(Context context) {
        database = LibraryManagerDatabase.getInstance(context);
    }

}
