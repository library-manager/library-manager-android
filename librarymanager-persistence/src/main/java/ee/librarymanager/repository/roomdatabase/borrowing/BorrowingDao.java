package ee.librarymanager.repository.roomdatabase.borrowing;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

@Dao
public interface BorrowingDao {
    @Insert
    void save(BorrowingEntity borrowingEntity);
}
