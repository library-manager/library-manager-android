package ee.librarymanager.repository.roomdatabase.book;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import ee.librarymanager.model.Book;

@Entity
public class BookEntity {
    @PrimaryKey
    @NonNull
    private String isbn;

    private String title;

    private String authors;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public static BookEntity fromBook(Book book) {
        final BookEntity bookEntity = new BookEntity();
        bookEntity.setIsbn(book.getIsbn());
        bookEntity.setTitle(book.getTitle());
        bookEntity.setAuthors(Joiner.on("|").join(book.getAuthors()));
        return bookEntity;
    }

    public Book toBook() {
        final Book book = new Book();
        book.setIsbn(getIsbn());
        book.setTitle(getTitle());
        final Iterable<String> authors = Splitter.on("|").split(getAuthors());
        book.setAuthors(Lists.newArrayList(authors));
        return book;
    }
}
