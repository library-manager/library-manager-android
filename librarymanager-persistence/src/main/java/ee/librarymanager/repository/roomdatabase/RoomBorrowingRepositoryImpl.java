package ee.librarymanager.repository.roomdatabase;


import android.content.Context;

import ee.librarymanager.model.Borrowing;
import ee.librarymanager.repository.BorrowingRepository;
import ee.librarymanager.repository.roomdatabase.borrowing.BorrowingEntity;

public class RoomBorrowingRepositoryImpl extends RepositoryImplBase implements BorrowingRepository {

    public RoomBorrowingRepositoryImpl(Context context) {
        super(context);
    }

    @Override
    public void save(Borrowing borrowing) {
        final BorrowingEntity borrowingEntity = BorrowingEntity.fromBorrowing(borrowing);
        database.borrowingDao().save(borrowingEntity);
    }
}
