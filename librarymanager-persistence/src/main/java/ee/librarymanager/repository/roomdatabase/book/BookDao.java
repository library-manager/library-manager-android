package ee.librarymanager.repository.roomdatabase.book;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

@Dao
public interface BookDao {
    @Insert
    void save(BookEntity bookEntity);

    @Query("SELECT * FROM bookentity WHERE isbn = :isbn LIMIT 1")
    BookEntity findByIsbn(String isbn);
}
