package ee.librarymanager.repository.roomdatabase.person;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

@Dao
public interface PersonDao {
    @Insert
    void save(PersonEntity personEntity);

    @Query("SELECT * FROM personentity WHERE idCode = :idCode LIMIT 1")
    PersonEntity findByIdCode(String idCode);
}
