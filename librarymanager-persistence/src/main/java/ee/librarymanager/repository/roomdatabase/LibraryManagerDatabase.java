package ee.librarymanager.repository.roomdatabase;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import ee.librarymanager.repository.roomdatabase.book.BookDao;
import ee.librarymanager.repository.roomdatabase.book.BookEntity;
import ee.librarymanager.repository.roomdatabase.borrowing.BorrowingDao;
import ee.librarymanager.repository.roomdatabase.borrowing.BorrowingEntity;
import ee.librarymanager.repository.roomdatabase.person.PersonDao;
import ee.librarymanager.repository.roomdatabase.person.PersonEntity;

@Database(exportSchema = false, entities = {BookEntity.class, PersonEntity.class, BorrowingEntity.class}, version = 1)
abstract class LibraryManagerDatabase extends RoomDatabase {
    private static LibraryManagerDatabase instance;

    static LibraryManagerDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context,
                LibraryManagerDatabase.class, "LibraryManager-database").build();
        }
        return instance;
    }

    abstract BookDao bookDao();

    abstract PersonDao personDao();

    abstract BorrowingDao borrowingDao();

}
