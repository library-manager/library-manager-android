package ee.librarymanager.repository.roomdatabase;


import android.content.Context;

import ee.librarymanager.model.Person;
import ee.librarymanager.repository.PersonRepository;
import ee.librarymanager.repository.roomdatabase.person.PersonEntity;

public class RoomPersonRepositoryImpl extends RepositoryImplBase implements PersonRepository {

    public RoomPersonRepositoryImpl(Context context) {
        super(context);
    }

    @Override
    public void save(Person person) {
        final PersonEntity personEntity = database.personDao().findByIdCode(person.getIdCode());
        if (personEntity == null) { // if it was not found
            database.personDao().save(PersonEntity.fromPerson(person));
        }
    }

    public Person findByIdCode(String idCode) {
        final PersonEntity personEntity = database.personDao().findByIdCode(idCode);
        return personEntity.toPerson();
    }
}
