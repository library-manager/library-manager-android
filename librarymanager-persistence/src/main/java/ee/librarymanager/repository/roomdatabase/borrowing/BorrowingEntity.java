package ee.librarymanager.repository.roomdatabase.borrowing;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import ee.librarymanager.model.Borrowing;
import ee.librarymanager.repository.roomdatabase.book.BookEntity;
import ee.librarymanager.repository.roomdatabase.person.PersonEntity;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = {
    @ForeignKey(entity = BookEntity.class,
        parentColumns = "isbn",
        childColumns = "bookId",
        onDelete = CASCADE),
    @ForeignKey(entity = PersonEntity.class,
        parentColumns = "idCode",
        childColumns = "personId",
        onDelete = CASCADE)})
public class BorrowingEntity {
    @PrimaryKey(autoGenerate = true)
    private Long id;
    private String bookId;
    private String personId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public static BorrowingEntity fromBorrowing(Borrowing borrowing) {
        final BorrowingEntity borrowingEntity = new BorrowingEntity();
        borrowingEntity.setBookId(borrowing.getBook().getIsbn());
        borrowingEntity.setPersonId(borrowing.getPerson().getIdCode());
        return borrowingEntity;
    }
}
