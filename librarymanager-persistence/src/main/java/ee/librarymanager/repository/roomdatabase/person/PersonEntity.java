package ee.librarymanager.repository.roomdatabase.person;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import ee.librarymanager.model.Person;

@Entity
public class PersonEntity {
    @PrimaryKey
    @NonNull
    private String idCode;
    private String firstName;
    private String lastName;

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public static PersonEntity fromPerson(Person person) {
        final PersonEntity personEntity = new PersonEntity();
        personEntity.setIdCode(person.getIdCode());
        personEntity.setFirstName(person.getFirstName());
        personEntity.setLastName(person.getLastName());
        return personEntity;
    }

    public Person toPerson() {
        final Person person = new Person();
        person.setIdCode(getIdCode());
        person.setFirstName(getFirstName());
        person.setLastName(getLastName());
        return person;
    }
}
